﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Office
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList employees = new ArrayList();
            ArrayList freelancers = new ArrayList();
            ArrayList orders = new ArrayList();

            Console.WriteLine("Creating Employees. Please wait..");

            CreateEmployees(employees);

            Console.Clear(); Console.WriteLine("Simulation workflow. Please wait..");

            for (int w = 0; w < Employee.WEEKS; w++)
            {
                for (int d = 0; d < Employee.DAYS; d++)
                {
                    if (d == 5 || d  == 6) continue;

                    for (int h = 0; h < Employee.HOURS; h++)
                    {
                        orders = GenetateOrders(employees);

                        SetOrders(orders, employees, freelancers);

                        foreach (Employee emp in employees) emp.Working(d, w);
                        foreach (Freelancer fre in freelancers) fre.Working(d, w);
                    }
                    foreach (Freelancer freel in freelancers) freel.GetDaySalary();
                }
                foreach (Employee emp in employees) emp.GetWeekSalary();
            }

            string doc_path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Reports";
            try { DirectoryInfo di = Directory.CreateDirectory(doc_path); }
            catch(Exception ex) { Console.WriteLine(ex); }

            GenerateTotalReport(employees, freelancers, doc_path);
            GenerateReportsForEmployeers(employees, doc_path);
            GenerateReportsForFreelancers(freelancers, doc_path);

            Console.Clear(); Console.WriteLine("Reports saved to {0} \nPlease press \"Enter\"", doc_path);
            Console.ReadLine();
        }

        static void CreateEmployees(ArrayList employees)
        {
            Random rnd = new Random();
            int employees_amount = rnd.Next(10, 100);

            PostName new_post;
            bool flagDirector = false;
            bool flagAccauntant = false;
            bool flagManadger = false;

            for (int i = 0; i < employees_amount; i++)
            {
                new_post = (PostName)rnd.Next((int)PostName.CLEANER + 1);

                if (!flagDirector && i >= employees_amount - 3) new_post = PostName.DIRECTOR;       //***
                if (!flagManadger && i >= employees_amount - 2) new_post = PostName.MANAGER;        // adding if no such employees
                if (!flagAccauntant && i >= employees_amount - 1) new_post = PostName.ACCOUNTANT;   //***

                Employee emp;
                switch (new_post)
                {
                    case (PostName.DIRECTOR): emp = new Director(); flagDirector = true; break;
                    case (PostName.ACCOUNTANT): emp = new Accauntant(); flagAccauntant = true; break;
                    case (PostName.CLEANER): emp = new Cleaner(); break;
                    case (PostName.MANAGER): emp = new Employee(); flagManadger = true; break;
                    default: emp = new Employee(); break;
                }

                emp.AddNewPosts();
                employees.Add(emp);
                                
                var t = Task.Run(async delegate { await Task.Delay(10); });    // delay for real random
                t.Wait();
            }
        }

        static ArrayList GenetateOrders(ArrayList employees)
        {
            ArrayList orders = new ArrayList();
            Random rnd = new Random();

            foreach (Employee emp in employees)
            {
                if (emp is Director)
                {
                    for (int i = 0; i < rnd.Next(1, 4); i++)
                    {
                        try
                        {
                            Director dir = (Director)emp;
                            orders.Add(dir.GiveOrder(rnd));
                        }
                        catch (Exception ex) { Console.WriteLine("type mismatch. " + ex.Message); }
                                                
                        var t = Task.Run(async delegate { await Task.Delay(10); });    // delay for real random
                        t.Wait();
                    }
                }
            }
            return orders;
        }

        static void SetOrders(ArrayList orders, ArrayList employees, ArrayList freelancers)
        {
            bool remove_order = false;

            foreach (Employee emp in employees)
            {
                Order tmp_order = new Order();
                remove_order = false;

                foreach (Order order in orders)
                {
                    if (emp.TakeOrder(order) == OrderStatus.ACCEPTED)
                    {
                        tmp_order = order; remove_order = true;
                    }
                }

                if (remove_order) orders.Remove(tmp_order);

                if (orders.Count == 0) break;
            }
            
            if (orders.Count != 0)
            {
                foreach (Order order in orders)
                {
                    if (order.get_duty != Duties.CLEANING)
                    {
                        if (freelancers.Count == 0)
                        {
                            Freelancer new_freelan = new Freelancer();
                            new_freelan.TakeOrder(order);
                            freelancers.Add(new_freelan);
                        }
                        else
                        {
                            bool new_freelancer = true;
                            foreach (Freelancer freelancer in freelancers)
                            {
                                if (freelancer.TakeOrder(order) == OrderStatus.ACCEPTED) { new_freelancer = false; break; }
                            }
                            if (new_freelancer)
                            {
                                Freelancer new_freelan = new Freelancer();
                                new_freelan.TakeOrder(order);
                                freelancers.Add(new_freelan);
                            }
                        }
                    }
                }
            }
        }
     
        static void GenerateTotalReport(ArrayList employees, ArrayList frelancers, string doc_path)
        {
            StreamWriter str_out;
            StringBuilder str = new StringBuilder();

            try { str_out = new StreamWriter(doc_path + @"\\Total report.txt"); }
            catch (IOException exc)
            {
                Console.WriteLine(exc.Message);
                return;
            }

            foreach (Employee emp in employees)
            {
                str.Append("Employee (id " + emp.get_id + ")\r\n");

                foreach (Post post in emp.get_posts)
                {
                    str.Append("\t Post: " + post.get_post + ";\r\n");
                }

                str.Append("\t Total salary: " + emp.get_total_salary + ";\r\n");
                if (emp is Director)
                {
                    try
                    {
                        Director dir = (Director)emp;
                        str.Append("\t Total given orders: " + dir.get_orders_amount + ";\r\n");
                    }
                    catch (InvalidCastException ex) { Console.WriteLine(ex.Message); }
                }
                else str.Append("\t Total recieved orders: " + emp.get_total_orders + ";\r\n");

                try { str_out.Write(str); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }

                str.Clear();
            }

            foreach (Freelancer freelancer in frelancers)
            {
                str.Append("Freelancer (id " + freelancer.get_id + ")\r\n");
                str.Append("\t Total salary: " + freelancer.get_total_salary + ";\r\n");
                str.Append("\t Total recieved orders: " + freelancer.get_total_orders + ";\r\n");

                try { str_out.Write(str); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }
                str.Clear();
            }
            str_out.Close();
        }

        static void GenerateReportsForEmployeers(ArrayList employees, string doc_path)
        {
            StreamWriter str_out;
            StringBuilder str = new StringBuilder();
            
            foreach (Employee emp in employees)
            {
                string file_name;
                int emp_count = emp.get_id;

                file_name = doc_path + @"\\Employee #" + emp_count + " (id" + emp.get_id + ").txt";
                try { str_out = new StreamWriter(file_name); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }

                str.Append("\r\n Employee #" + emp_count + ", (id " + emp.get_id + ")\r\n");

                foreach (Post post in emp.get_posts) { str.Append("\t Post: " + post.get_post + ";\r\n"); }

                for (int w = 0; w < Employee.WEEKS; w++)
                {
                    for (int d = 0; d < Employee.DAYS; d++)
                    {
                        str.Append("\t Day: " + (w * 7 + d + 1) + "\r\n");

                        if (emp is Director) str.Append("\t\t Give orders \r\n");
                        else if (emp.get_orders[w * 7 + d].Count == 0) str.Append("\t\t Order: No orders on this day \r\n");

                        foreach (Order order in emp.get_orders[w * 7 + d])
                        {
                            str.Append("\t\t Order: " + order.get_duty + ";" + " execution time: " + order.exec_time + " hour(s)" + "\r\n");
                        }
                    }
                    str.Append("\t Salary for week : " + emp.get_week_salary[w] + ";\r\n" + "\r\n");

                }
                str.Append("\t Total salary: " + emp.get_total_salary + ";\r\n");
                str.Append("\t Total orders: " + emp.get_total_orders + ";\r\n" + "\r\n");


                try { str_out.Write(str); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }
                str.Clear();
                str_out.Close();
            }
        }

        static void GenerateReportsForFreelancers(ArrayList freelancers, string doc_path)
        {   
            StreamWriter str_out;
            StringBuilder str = new StringBuilder();

            int freel_count = 0;

            foreach (Freelancer freel in freelancers)
            {
                string file_name;
                freel_count++;

                file_name = doc_path + @"\\Freelancer #" + freel_count + " (id" + freel.get_id + ").txt";
                try { str_out = new StreamWriter(file_name); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }

                str.Append("\r\n Freelancer #" + freel_count + ", (id " + freel.get_id + ")\r\n");
                
                for (int w = 0; w < Employee.WEEKS; w++)
                {
                    for (int d = 0; d < Employee.DAYS; d++)
                    {
                        str.Append("\t Day: " + (w * 7 + d + 1) + "\r\n");

                        if (freel.get_orders[w * 7 + d].Count == 0) str.Append("\t\t Order: No orders on this day \r\n");

                        foreach (Order order in freel.get_orders[w * 7 + d])
                        {
                            str.Append("\t\t Order: " + order.get_duty + ";" + " execution time: " + order.exec_time + " hour(s)" + "\r\n");
                        }
                        str.Append("\t Salary for day : " + freel.get_day_salary[w*7+1] + ";\r\n" + "\r\n");
                    }
                }
                str.Append("\t Total salary: " + freel.get_total_salary + ";\r\n");
                str.Append("\t Total orders: " + freel.get_total_orders + ";\r\n" + "\r\n");
                
                try { str_out.Write(str); }
                catch (IOException exc)
                {
                    Console.WriteLine(exc.Message);
                    return;
                }
                str.Clear();
                str_out.Close();
            }
        }
    }
}
