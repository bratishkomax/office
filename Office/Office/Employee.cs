﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office
{
    class Employee
    {
        public const int HOURS = 8;
        public const int DAYS = 7;
        public const int WEEKS = 4;

        private static int day;
        private static int week;
        
        private int employee_id;

        protected Order order;
        protected List<Post> posts;
        protected List<Order>[] orders;                
                
        protected int total_salary = 0;

        private static int employee_counter = 0;
        private int total_orders = 0;

        protected int[] salary_per_week = new int[WEEKS];

        public List<Post> get_posts { get { return posts; } }
        public List<Order>[] get_orders { get { return orders; } }
        public int[] get_week_salary { get { return salary_per_week; } }
        public int get_total_salary { get { return total_salary; } }
        public int get_total_orders { get { return total_orders; } }
        public int get_id { get { return employee_id; } }
        public int get_day_num { get { return day * week + day; } }
        public int get_week { get { return week; } }

        public Employee()
        {
            employee_counter++;
            posts = new List<Post>();
            orders = new List<Order>[DAYS*WEEKS];
            for (int i = 0; i < orders.Length; i++) orders[i] = new List<Order>();
            order = new Order();
            employee_id = employee_counter;
        }
                
        public virtual void AddNewPosts()
        {
            PostName new_post;
            int post_amount;
            bool break_for = false;

            Random rnd = new Random();
            post_amount = rnd.Next(1,5);

            for (int i = 0; i < post_amount; i++)
            {
                new_post = (PostName)rnd.Next(3, 6);
                foreach (Post p in this.posts)
                    if (p.get_post == new_post) break_for = true;
                if (break_for) { i--; break; }
                posts.Add(new Post(new_post));
            }
        }

        public virtual OrderStatus TakeOrder(Order new_order)
        {
            bool discard_order = true;
               
            if (order.status == OrderStatus.IN_PROGRESS) return OrderStatus.DISCARDED;
            
            foreach (Post post in posts)
            {
                if(post.get_duty == new_order.get_duty) { discard_order = false; break; }
            }
            if (discard_order) return OrderStatus.DISCARDED;

            if (order.status == OrderStatus.ACCEPTED)
                if (new_order.get_priority < order.get_priority && new_order.get_salary < order.get_salary) return OrderStatus.DISCARDED;
            
            order = new_order;
            order.status = OrderStatus.ACCEPTED;
            return OrderStatus.ACCEPTED;
        }

        public void Working(int day, int week)
        {
            Employee.day = day;
            Employee.week = week;

            if (order.exec_time >= 2 && order.status != OrderStatus.IN_PROGRESS)
            {
                order.exec_time--;
                order.status = OrderStatus.IN_PROGRESS;
            }
            else if (order.status == OrderStatus.IN_PROGRESS || order.status == OrderStatus.ACCEPTED)
            {
                order.status = OrderStatus.FREE;
                orders[week*7+day].Add(order);
                total_orders++;
            }            
        }

        public virtual void GetWeekSalary()
        {
            bool post_manager = false;

            for (int d = 0; d < Employee.DAYS; d++)
            {
                foreach (Order order in orders[get_week*7 + d])
                {
                    if (order.get_duty == Duties.MANAGING) { post_manager = true; continue; }
                    salary_per_week[get_week] += order.get_salary*order.exec_time;
                }
            }
            
            if (post_manager == true) salary_per_week[get_week] += (int)Salaries.MANAGER;

            total_salary += salary_per_week[get_week];
        }
        
    }

    class Post
    {
        private PostName post_name;
        private Duties duty;
        
        public PostName get_post { get { return post_name; } }
        public Duties get_duty { get { return duty; } }
        
        public Post(PostName post_name)
        {
            this.post_name = post_name;

            duty = (Duties)(int)post_name;
        }
    }

    class Order
    {
        private int priority;
        private int execution_time;
        private int salary;
        private int[] salaries =  { (int)Salaries.DIRECTOR,
                                    (int)Salaries.ACCOUNTANT,
                                    (int)Salaries.MANAGER,
                                    (int)Salaries.PROGRAMMER,
                                    (int)Salaries.DESIGNER,
                                    (int)Salaries.TESTER,
                                    (int)Salaries.CLEANER,
                                    
                                    (int)Salaries.ACCOUNTANT_FREELANCER,
                                    (int)Salaries.MANAGER_FREELANCER,
                                    (int)Salaries.PROGRAMMER_FREELANCER,
                                    (int)Salaries.DESIGNER_FREELANCER,
                                    (int)Salaries.TESTER_FREELANCER, };
        protected Duties duty;

        public Duties get_duty { get { return duty; } }
        public int get_priority { get { return priority; } }
        public int get_salary { get { return salary; } }
        public int exec_time { set { execution_time = exec_time; } get { return execution_time; } }
        public OrderStatus status;

        public Order(Duties duty, int priority, int execution_time)
        {
            this.duty = duty;
            this.priority = priority;
            this.execution_time = execution_time;

            salary = salaries[(int)duty];
            status = OrderStatus.FREE;
        }
        public Order()
        {
            duty = 0;
            priority = 0;
            execution_time = 0;
            salary = 0;
            status = OrderStatus.FREE;
        }
        public void set_freelancer_salary()
        {
            salary = salaries[(int)duty+6];
        }
    }

    class Director : Employee
    {
        public Director() : base() { }

        private int orders_amount;
        public int get_orders_amount { get { return orders_amount; } }

        public override void AddNewPosts()
        {
            int post_amount;
            Random rnd = new Random();

            post_amount = rnd.Next(3);
            posts.Add(new Post(PostName.DIRECTOR));
            if (post_amount < 1) posts.Add(new Post(PostName.MANAGER));
        }

        public Order GiveOrder(Random rnd)
        {
            Duties duty;
            int priority;
            int exec_time;

            duty = (Duties)rnd.Next(1, (int)Duties.CLEANING + 1);
            priority = rnd.Next(1, 4);
            exec_time = rnd.Next(1, 3);
            orders_amount++;
            return new Order(duty, priority, exec_time);
        }
        
        public override void GetWeekSalary()
        {
            salary_per_week[get_week] = (int)Salaries.DIRECTOR;
            if (posts.Count > 1) salary_per_week[get_week] += (int)Salaries.MANAGER;
            total_salary += salary_per_week[get_week];
        }
    }

    class Accauntant : Employee
    {
        public Accauntant() : base() { }

        public override void AddNewPosts()
        {
            int post_amount;
            Random rnd = new Random();

            post_amount = rnd.Next(3);
            posts.Add(new Post(PostName.ACCOUNTANT));
            if (post_amount < 1) posts.Add(new Post(PostName.MANAGER));
        }

        public override void GetWeekSalary()
        {
            salary_per_week[get_week] = (int)Salaries.ACCOUNTANT;
            if (posts.Count > 1) salary_per_week[get_week] += (int)Salaries.MANAGER;
            total_salary += salary_per_week[get_week];
        }
    }

    class Cleaner : Employee
    {
        public Cleaner() : base() { }

        public override void AddNewPosts()
        {
            posts.Add(new Post(PostName.CLEANER));
        }
    }

    class Freelancer : Employee
    {
        private int[] orders_per_day = new int[WEEKS*DAYS];
        private int[] salary_per_day = new int[WEEKS*DAYS];

        public int[] get_day_salary { get { return salary_per_day; } }

        public Freelancer() : base() { }

        public override OrderStatus TakeOrder(Order new_order)
        {
            if (order.status == OrderStatus.IN_PROGRESS || order.status == OrderStatus.ACCEPTED) return OrderStatus.DISCARDED;
            else
            {
                order = new_order;
                order.set_freelancer_salary();
                order.status = OrderStatus.ACCEPTED;
                orders_per_day[get_day_num]++;

                return OrderStatus.ACCEPTED;
            }
        }
        
        public void GetDaySalary()
        {
            int order_counter = orders_per_day[get_day_num];

            foreach (Order order in orders[get_day_num])
            {
                salary_per_day[get_day_num] += order.get_salary;
            }
            total_salary += salary_per_day[get_day_num];
        }        
    }

    enum OrderStatus
    {
        ACCEPTED,
        DISCARDED,
        IN_PROGRESS,
        FREE        
    };

    enum PostName
    {
        DIRECTOR,
        ACCOUNTANT,
        MANAGER,
        PROGRAMMER,
        DESIGNER,
        TESTER,
        CLEANER,
        FREELANCER
    };

    enum Duties
    {        
        ACCOUNTANTING=1,
        MANAGING,
        PROGRAMMING,
        DESIGNING,
        TESTING,
        CLEANING
    };

    enum Salaries
    {
        DIRECTOR = 1500,
        ACCOUNTANT = 500,
        MANAGER = 600,
        PROGRAMMER = 25,
        DESIGNER = 20,
        TESTER = 21,
        CLEANER = 10,

        ACCOUNTANT_FREELANCER = 22,
        MANAGER_FREELANCER = 23,
        PROGRAMMER_FREELANCER = 26,
        DESIGNER_FREELANCER = 25,
        TESTER_FREELANCER = 24,
    };
}
